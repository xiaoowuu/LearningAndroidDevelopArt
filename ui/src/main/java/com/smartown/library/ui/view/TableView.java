package com.smartown.library.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.smartown.library.ui.R;
import com.smartown.library.ui.utils.ScreenUtil;

/**
 * Author:Tiger
 * <p>
 * CrateTime:2016-12-08 18:30
 * <p>
 * Description:统计表格控件
 */
public class TableView extends View {

    private int columnCount;
    private int rowCount;
    private float columnWidth;
    private float rowHeight;
    private int headerColor;
    private float dividerWidth;
    private int dividerColor;
    private float textSize;
    private int textColor;

    private Paint paint;

    public TableView(Context context) {
        super(context);
        init(null);
    }

    public TableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TableView);
            columnCount = typedArray.getInt(R.styleable.TableView_TableView_columnCount, 1);
            rowCount = typedArray.getInt(R.styleable.TableView_TableView_rowCount, 0) + 1;
            columnWidth = typedArray.getDimensionPixelSize(R.styleable.TableView_TableView_columnWidth, 0);
            rowHeight = typedArray.getDimensionPixelSize(R.styleable.TableView_TableView_rowHeight, ScreenUtil.dip2px(getContext(), 36));
            headerColor = typedArray.getColor(R.styleable.TableView_TableView_headerColor, Color.parseColor("#00ffffff"));
            dividerWidth = typedArray.getDimensionPixelSize(R.styleable.TableView_TableView_dividerWidth, 1);
            dividerColor = typedArray.getColor(R.styleable.TableView_TableView_dividerColor, Color.parseColor("#E1E1E1"));
            textSize = typedArray.getDimensionPixelSize(R.styleable.TableView_TableView_textSize, ScreenUtil.dip2px(getContext(), 10));
            textColor = typedArray.getColor(R.styleable.TableView_TableView_textColor, Color.parseColor("#999999"));
            typedArray.recycle();
        } else {
            columnCount = 1;
            rowCount = 1;
            columnWidth = 0;
            rowHeight = ScreenUtil.dip2px(getContext(), 36);
            headerColor = Color.parseColor("#00ffffff");
            dividerWidth = 1;
            dividerColor = Color.parseColor("#E1E1E1");
            textSize = ScreenUtil.dip2px(getContext(), 10);
            textColor = Color.parseColor("#999999");
        }
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float width;
        if (columnWidth == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            width = getMeasuredWidth();
            if (columnWidth == 0) {
                columnWidth = (width - (columnCount + 1) * dividerWidth) / columnCount;
            }
        } else {
            width = (dividerWidth + columnWidth) * columnCount + dividerWidth;
        }
        //计算高度
        float height = (dividerWidth + rowHeight) * rowCount + dividerWidth;
        setMeasuredDimension((int) width, (int) height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawHeader(canvas);
        drawFramework(canvas);
        drawContent(canvas);
    }

    /**
     * 画表头
     *
     * @param canvas
     */
    private void drawHeader(Canvas canvas) {
        paint.setColor(headerColor);
        canvas.drawRect(dividerWidth, dividerWidth, getWidth() - dividerWidth, rowHeight + dividerWidth, paint);
    }

    /**
     * 画整体表格框架
     */
    private void drawFramework(Canvas canvas) {
        paint.setColor(dividerColor);
        for (int i = 0; i < columnCount + 1; i++) {
            canvas.drawRect(i * (columnWidth + dividerWidth), 0, i * (columnWidth + dividerWidth) + dividerWidth, getHeight(), paint);
        }
        for (int i = 0; i < rowCount + 1; i++) {
            canvas.drawRect(0, i * (rowHeight + dividerWidth), getWidth(), i * (rowHeight + dividerWidth) + dividerWidth, paint);
        }
    }

    /**
     * 画内容
     */
    private void drawContent(Canvas canvas) {
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                canvas.drawText((i + 1) + "/" + (j + 1),
                        j * (columnWidth + dividerWidth) + columnWidth / 2,
                        getTextBaseLine(i * (rowHeight + dividerWidth), paint),
                        paint);
            }
        }
    }

    private float getTextBaseLine(float rowStart, Paint paint) {
        final Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return (rowStart + (rowStart + rowHeight) - fontMetrics.bottom - fontMetrics.top) / 2;
    }

}
