package com.smartown.library.ui.utils;

import android.content.Context;

/**
 * Author:Tiger[https://github.com/KungFuBrother]
 * <p>
 * CrateTime:2016-12-8 22:27:13
 * <p>
 * Description:JumpUtils
 */
public class ScreenUtil {

    public static int dip2px(Context context, int dpSize) {
        return (int) (context.getResources().getDisplayMetrics().density*dpSize);
    }

}
