package com.smartown.learning.androiddevelopart;

import com.smartown.library.ui.base.BaseActivity;
import com.smartown.library.ui.utils.JumpUtils;

public class MainActivity extends BaseActivity {

    @Override
    protected void init() {
        JumpUtils.jump(this, getString(R.string.menu), TableFragment.class);
        finish();
    }

    @Override
    protected void findViews() {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void registerViews() {

    }
}
