// IBookManager.aidl
package com.smartown.learning.androiddevelopart.chapters.chapter2.section3.part3;

import com.smartown.learning.androiddevelopart.chapters.chapter2.section3.part3.Book;

// Declare any non-default types here with import statements

interface IBookManager {

    List<Book> getBookList();

    void addBook(Book book);

}
