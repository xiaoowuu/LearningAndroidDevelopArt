package com.smartown.learning.androiddevelopart.menu.entity;

import java.util.List;

/**
 * Author:Smartown
 * Date:2016/11/26 15:03
 * Description:
 */
public class Menu {

    private List<Chapter> chapters;

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }
}
