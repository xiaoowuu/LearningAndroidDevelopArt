package com.smartown.learning.androiddevelopart.chapters.chapter3.section1.part4;

import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.LinearLayout;

import com.smartown.learning.androiddevelopart.R;
import com.smartown.library.ui.base.BaseFragment;

/**
 * Author:Smartown
 * Date:2016/12/4 10:40
 * Description:
 */
public class VelocityTrackerFragment extends BaseFragment {

    private LinearLayout layout;
    private VelocityTracker velocityTracker;

    @Override
    protected void init() {
        findViews(R.layout.fragment_3_1_4_velocity_tracker);
        velocityTracker = VelocityTracker.obtain();
    }

    @Override
    public void onDestroy() {
        velocityTracker.clear();
        velocityTracker.recycle();
        super.onDestroy();
    }

    @Override
    protected void findViews() {
        layout = (LinearLayout) findViewById(R.id.layout);
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void registerViews() {
        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                velocityTracker.addMovement(event);
                velocityTracker.computeCurrentVelocity(1);
                Log.i("VelocityTracker", velocityTracker.getXVelocity() + " " + velocityTracker.getYVelocity());
                return true;
            }
        });
    }
}
