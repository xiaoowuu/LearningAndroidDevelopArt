package com.smartown.learning.androiddevelopart.menu.entity;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import java.util.List;

/**
 * Author:Smartown
 * Date:2016/11/26 15:04
 * Description:
 */
public class Chapter {

    private String title;
    private String description;
    private String fragment;
    private List<Chapter> chapters;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Class getFragment() {
        if (!TextUtils.isEmpty(fragment)) {
            try {
                return Class.forName(fragment);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return Fragment.class;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }
}
