package com.smartown.learning.androiddevelopart.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.smartown.learning.androiddevelopart.R;
import com.smartown.learning.androiddevelopart.menu.entity.Chapter;
import com.smartown.learning.androiddevelopart.menu.entity.Menu;
import com.smartown.library.ui.base.BaseFragment;
import com.smartown.library.ui.utils.JumpUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Author:Smartown
 * Date:2016/11/26 9:26
 * Description:
 */
public class MenuFragment extends BaseFragment implements View.OnClickListener {

    private int index1 = -1;
    private int index2 = -1;
    private int index3 = -1;

    private RecyclerView recyclerView;
    private Menu menu;
    private SimpleTextAdapter adapter;
    private List<Chapter> chapters;

    @Override
    protected void init() {
        try {
            menu = new Gson().fromJson(getAssetsMenu(), Menu.class);
            Bundle bundle = getArguments();
            if (bundle != null) {
                if (bundle.containsKey("index1")) {
                    index1 = bundle.getInt("index1");
                }
                if (bundle.containsKey("index2")) {
                    index2 = bundle.getInt("index2");
                }
                if (bundle.containsKey("index3")) {
                    index3 = bundle.getInt("index3");
                }
            }
            if (index1 == -1) {
                chapters = menu.getChapters();
            } else {
                if (index2 == -1) {
                    chapters = menu.getChapters().get(index1).getChapters();
                } else {
                    if (index3 == -1) {
                        chapters = menu.getChapters().get(index1).getChapters().get(index2).getChapters();
                    } else {
                        chapters = menu.getChapters().get(index1).getChapters().get(index2).getChapters().get(index3).getChapters();
                    }
                }
            }
            adapter = new SimpleTextAdapter() {
                @Override
                public Context getContext() {
                    return getActivity();
                }

                @Override
                public void onBindViewHolder(Item holder, int position) {
                    final Chapter chapter = chapters.get(position);
                    holder.textView.setText(chapter.getTitle() + "\t" + chapter.getDescription());
                    holder.itemView.setTag(position);
                    holder.itemView.setOnClickListener(MenuFragment.this);
                }

                @Override
                public int getItemCount() {
                    return chapters.size();
                }
            };
        } catch (IOException e) {
            e.printStackTrace();
        }
        findViews(R.layout.fragment_list);
    }

    @Override
    protected void findViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    @Override
    protected void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new SimpleTextAdapter.ItemDivider());
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void registerViews() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                final int position = (int) v.getTag();
                final Chapter chapter = chapters.get(position);
                Bundle bundle = new Bundle();
                if (index1 == -1) {
                    bundle.putInt("index1", position);
                } else {
                    if (index2 == -1) {
                        bundle.putInt("index1", index1);
                        bundle.putInt("index2", position);
                    } else {
                        bundle.putInt("index1", index1);
                        bundle.putInt("index2", index2);
                        bundle.putInt("index3", position);
                    }
                }
                JumpUtils.jump(getActivity(), chapter.getTitle(), chapter.getFragment(), bundle);
                break;
        }
    }

    private String getAssetsMenu() throws IOException {
        final InputStream inputStream = getActivity().getAssets().open("menu.json");
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int length;
        while ((length = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, length);
        }
        outputStream.close();
        inputStream.close();
        return outputStream.toString();
    }

}
