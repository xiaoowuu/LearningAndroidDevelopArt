package com.smartown.learning.androiddevelopart.chapters.chapter3.section1.part4;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.smartown.learning.androiddevelopart.R;
import com.smartown.library.ui.base.BaseFragment;

/**
 * Author:Smartown
 * Date:2016/12/4 11:43
 * Description:
 */
public class GestureDetectorFragment extends BaseFragment implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private LinearLayout layout;
    private GestureDetector gestureDetector;

    @Override
    protected void init() {
        findViews(R.layout.fragment_3_1_4_velocity_tracker);
        gestureDetector = new GestureDetector(getActivity(), this);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    protected void findViews() {
        layout = (LinearLayout) findViewById(R.id.layout);
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void registerViews() {
        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        Log.i("GestureDetector", "onSingleTapConfirmed");
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        Log.i("GestureDetector", "onDoubleTap");
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        Log.i("GestureDetector", "onDoubleTapEvent");
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        Log.i("GestureDetector", "onDown");
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Log.i("GestureDetector", "onShowPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        Log.i("GestureDetector", "onSingleTapUp");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.i("GestureDetector", "onScroll");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.i("GestureDetector", "onLongPress");
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Log.i("GestureDetector", "onFling");
        return false;
    }
}
