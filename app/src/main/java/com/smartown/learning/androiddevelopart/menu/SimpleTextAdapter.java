package com.smartown.learning.androiddevelopart.menu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Author:Smartown
 * Date:2016/11/26 9:52
 * Description:
 */
public abstract class SimpleTextAdapter extends RecyclerView.Adapter<SimpleTextAdapter.Item> {

    public abstract Context getContext();

    @Override
    public Item onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Item(new TextView(getContext()));
    }

    public final static class Item extends RecyclerView.ViewHolder {

        public TextView textView;

        public Item(View itemView) {
            super(itemView);
            final int screenWidth = itemView.getContext().getResources().getDisplayMetrics().widthPixels;
            final float density = itemView.getContext().getResources().getDisplayMetrics().density;
            textView = (TextView) itemView;
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
            textView.setPadding((int) (density * 16), 0, (int) (density * 16), 0);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setLines(1);
            textView.setBackgroundColor(Color.parseColor("#ffffff"));
            itemView.setLayoutParams(new RecyclerView.LayoutParams(screenWidth, (int) (density * 48)));
        }
    }

    public static class ItemDivider extends RecyclerView.ItemDecoration {

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);
        }

        public ItemDivider() {
            super();
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(0, 0, 0, 1);
        }
    }

}
