package com.smartown.learning.androiddevelopart;

import com.smartown.library.ui.base.BaseFragment;

/**
 * Author:Smartown
 * Date:2016/12/8 22:31
 * Description:
 */
public class TableFragment extends BaseFragment {
    @Override
    protected void init() {
        findViews(R.layout.fragment_table);
    }

    @Override
    protected void findViews() {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void registerViews() {

    }
}
