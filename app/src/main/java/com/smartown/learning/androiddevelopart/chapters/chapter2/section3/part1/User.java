package com.smartown.learning.androiddevelopart.chapters.chapter2.section3.part1;

import java.io.Serializable;

/**
 * Author:Smartown
 * Date:2016/11/28 22:47
 * Description:
 */
public class User implements Serializable {

    private final static long serialVersionUID = 1L;

    public int userId;
    public String userName;
    public boolean isMale;

    public User(int userId, String userName, boolean isMale) {
        this.userId = userId;
        this.userName = userName;
        this.isMale = isMale;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", isMale=" + isMale +
                '}';
    }
}
