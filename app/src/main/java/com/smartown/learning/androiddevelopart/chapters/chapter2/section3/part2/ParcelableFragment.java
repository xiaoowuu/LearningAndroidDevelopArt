package com.smartown.learning.androiddevelopart.chapters.chapter2.section3.part2;

import android.content.Intent;
import android.widget.TextView;

import com.smartown.learning.androiddevelopart.R;
import com.smartown.library.ui.base.BaseFragment;

/**
 * Author:Smartown
 * Date:2016-12-3 10:30:50
 * Description:2.3.2 Parcelable接口
 */
public class ParcelableFragment extends BaseFragment {

    private TextView originTextView;
    private TextView resultTextView;

    @Override
    protected void init() {
        findViews(R.layout.fragment_2_3_1);
    }

    @Override
    protected void findViews() {
        originTextView = (TextView) findViewById(R.id.serializable_origin);
        resultTextView = (TextView) findViewById(R.id.serializable_result);
    }

    @Override
    protected void initViews() {
        final User user = new User(1, "tiger", true);
        originTextView.setText(user.toString());

        Intent intent = new Intent();
        intent.putExtra("user", user);

        final User newUser = intent.getParcelableExtra("user");
        resultTextView.setText(newUser.toString());
    }

    @Override
    protected void registerViews() {

    }
}
